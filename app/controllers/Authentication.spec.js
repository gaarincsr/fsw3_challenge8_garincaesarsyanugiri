const authController = require('./ApplicationController');
const { sequelize } = require('../models');

const { queryInterface } = sequelize;

beforeAll(async () => {
    const password = await authController.hashPassword('123456');

    await queryInterface.bulkInsert('users', [
        {
            name: 'Raden',
            email: 'raechan@mail.com',
            password,
            createdAt: new Date(),
            updatedAt: new Date(),
        }
    ], {});
});

afterAll(async () => {
    await queryInterface.bulkDelete('users', null, {
        truncate: true,
        restartIdentity: true,
    });
});

describe('Authentication Controller', () => {
    describe('#login()', () => {
        it('should return success 200 and token', async () => {
            const email = 'raechan@mail.com';
            const password = '12345';

            const mockRequest = {
                body: {
                    email,
                    password,
                }
            };

            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis(),
            };

            await authController.handleLogin(mockRequest, mockResponse);

            expect(mockResponse.status).toHaveBeenCalledWith(201);
            expect(mockResponse.json).toHaveBeenCalledWith({
                accesstoken: expect.any(String),
            });
        });

        it('should return error 404 and message', async () => {
            const email = 'asalasalan@mail.com';
            const password = 'asal123';

            const mockRequest = {
                body: {
                    email,
                    password,
                }
            };

            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis(),
            };

            await authController.handleLogin(mockRequest, mockResponse);

            expect(mockResponse.status).toHaveBeenCalledWith(404);
            expect(mockResponse.json).toHaveBeenCalledWith({
                status: 'Error',
                message: expect.any(String),
            });
        })

        it('should return error 401 and message', async () => {
            const email = 'raechan@mail.com';
            const password = 'asal123';

            const mockRequest = {
                body: {
                    email,
                    password,
                }
            };

            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis(),
            };

            await authController.handleLogin(mockRequest, mockResponse);

            expect(mockResponse.status).toHaveBeenCalledWith(401);
            expect(mockResponse.json).toHaveBeenCalledWith({
                status: 'Error',
                message: expect.any(String),
            });
        })
    })

    describe('#register', () => {
        it('should return 201 and token', async () => {
            const email = 'bobby@mail.com';
            const password = 'bobby123';
            const name = 'Bobby';

            const mockRequest = {
                body: {
                    email,
                    password,
                    name,
                }
            };

            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis(),
            };

            await authController.handleRegister(mockRequest, mockResponse);

            expect(mockResponse.status).toHaveBeenCalledWith(201);
            expect(mockResponse.json).toHaveBeenCalledWith({
                accesstoken: expect.any(String),
            });
        });

        it('should return error 422 and message', async () => {
            const email = 'raechan@mail.com';
            const password = '12345';
            const name = 'Raden';

            const mockRequest = {
                body: {
                    email,
                    password,
                    name,
                }
            };

            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis(),
            };

            await authController.handleRegister(mockRequest, mockResponse);

            expect(mockResponse.status).toHaveBeenCalledWith(42);
            expect(mockResponse.json).toHaveBeenCalledWith({
                status: 'Error',
                message: expect.any(String),
            });
        });
    });
});